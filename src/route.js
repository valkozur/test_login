import Vue from 'vue'
import VueRouter from 'vue-router'
import App from "@/App";
import noPage from "@/components/404.vue";

Vue.use(VueRouter)
const routes =[
    {
        path: '/',
        component: App,
        name: 'home'
    },
    {
        path: '/no-page',
        component: noPage,
        name: 'no-page'
    },

]

export default new VueRouter({
    routes
})